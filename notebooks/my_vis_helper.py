
def image_title(r, col = 'None'):
    of_intrest = ['Diet','description', 'imageUrl', 'FoodType', 'complexity', 'attractiveness', 'storability' , 'preparationTime','price']
    
    attr = [ 'Ingredients', 'complexity', 'attractiveness', 'storability' , 'preparationTime','price' ]
    attr = [ "<div style='font-size:12px'  > <strong> %s </strong> : %s </div>" % (k,str(r[k])) for k in attr ]
    
    block = """
    <div style=' background-color:%s ; padding:10px; margin:10px;   width:900px ; overflow:hidden;'  >
        
        <div style='width: width:350px; max-height: 300px; overflow:hidden;  float:left;' > 
            <img src=%s style=' margin-right:0px; margin-bottom:5px; width:350px; box-shadow: 1px 1px 2px rgba(0,0,0,0.5); clip: rect(0px,350px,100px,0px);' >
        </div> 
        
        <div style=' padding-left:20px; width:500px ; float: left' > 
            <div style='font-size:10px' > Score : %s </div>
            <div style='font-size:17px' > <strong> %s </strong> </div>
            <div style='font-size:10px'  > Id : %s </div>
            %s
        </div>
    </div>
    """ 
    
    return block % ( col , r['imageUrl'], '%s', r['name'].upper() ,r['recipeId'] , ''.join(attr) )


def view_list(results):
    return  ''.join([ image_title(r['_source']) % r['_score'] for r in results ])

