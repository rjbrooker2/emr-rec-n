from pyspark.mllib.linalg import Vectors

convert_vector = lambda x: " ".join(["%s|%s" % (i, v) for i, v in enumerate(x)])
reverse_convert = lambda s: [float(f.split("|")[1]) for f in s.split(" ")]
vector_to_struct = lambda x, version, ts: ( convert_vector(x) , version, ts)

def fn_query(query_vec, q="*", cosine=False):
    query = {
        "query": {
            "function_score": {
                "query" : {  "query_string": { "query": q } },
                "script_score": {
                    "script": {
                            "inline": "payload_vector_score",
                            "lang": "native",
                            "params": { "field": "@model.factor", "vector": query_vec, "cosine" : cosine }
                        }
                },
                "boost_mode": "replace"
            }
        }
    }    
    return query


def get_user_recs(es, the_id, q="*", num=10, index="recipe-rec"):
    response = es.get(index=index, doc_type="users", id=the_id)
    src = response['_source']
    if '@model' in src and 'factor' in src['@model']:
        raw_vec = src['@model']['factor']
        # our script actually uses the list form for the query vector and handles conversion internally
        query_vec = reverse_convert(raw_vec)
        q = fn_query(query_vec, q=q, cosine=False)
        results = es.search(index, "recipes", body=q)
        hits = results['hits']['hits']
        return src, hits[:num]