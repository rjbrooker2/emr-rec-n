echo "Installing Java..."
sudo java -version
sudo apt-get install default-jre
sudo apt-get install default-jdk

echo "Installing pip..."
sudo apt-get install python-pip 
sudo apt-get install python3-pip

echo "Install python packages..." 
sudo python3 -m pip install --upgrade pip
sudo python3 -m pip install elasticsearch
sudo python3 -m pip install numpy
sudo python3 -m pip install jupyter
sudo python3 -m pip install pandas
sudo python3 -m pip install xlrd
sudo python3 -m pip install pymysql
sudo pip install stop-words
echo "Installing the ES python API..." 
sudo python3 -m pip install elasticsearch

echo "Navigate to the user home." 
cd ~/
pwd 

echo "Download and unzip Elasticsearch."
wget -nc https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-5.3.0.tar.gz
tar xfz --keep-old-files elasticsearch-5.3.0.tar.gz

echo 'Install the elasticsearch-vector-scoring plugin.'
./elasticsearch-5.3.0/bin/elasticsearch-plugin install https://github.com/MLnick/elasticsearch-vector-scoring/releases/download/v5.3.0/elasticsearch-vector-scoring-5.3.0.zip


echo "Download the Elasticsearch Spark connector from the Elasticsearch Hadoop project."
wget -nc http://download.elastic.co/hadoop/elasticsearch-hadoop-5.3.0.zip
unzip -o elasticsearch-hadoop-5.3.0.zip


echo "Download Apache Spark"
wget -nc http://mirrors.ukfast.co.uk/sites/ftp.apache.org/spark/spark-2.2.0/spark-2.2.0-bin-hadoop2.7.tgz
tar -xvzf --keep-old-files spark-2.2.0-bin-hadoop2.7.tgz

echo "Add variables to bash "
echo "" >> $HOME/.bashrc
echo "export PYTHONPATH=/usr/bin/python3" >> $HOME/.bashrc
echo "export SPARK_HOME=$HOME/spark-2.2.0-bin-hadoop2.7" >> $HOME/.bashrc
echo "export PYSPARK_PYTHON=/usr/bin/python3" >> $HOME/.bashrc
echo "export PYSPARK_DRIVER_PYTHON=/usr/bin/python3" >> $HOME/.bashrc
echo "export PYSPARK_DRIVER_PYTHON_OPTS=console" >> $HOME/.bashrc
echo "export PATH=/bin:\$PATH" >> $HOME/.bashrc

echo "Setup the variables"
export PYTHONPATH=/usr/bin/python3
export SPARK_HOME=$HOME/spark-2.2.0-bin-hadoop2.7
export PYSPARK_PYTHON=/usr/bin/python3
export PYSPARK_DRIVER_PYTHON=/usr/bin/python3
export PYSPARK_DRIVER_PYTHON_OPTS=console
export PATH=/bin:\$PATH



