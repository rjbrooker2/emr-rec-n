
# ---------------------------------------------------------------
# SCALABLE RECIPE RECCOMENDATIONS IN SPARK
# ---------------------------------------------------------------

# 1. Overview 

A scalable content-based recommendation engine, based on learnt user-item cosine similarity. 

It contains the following steps, 

1. Loads, formats and scales the recipe attributes
2. Removes noise and redundancy using dimension reduction.
3. Learns the corresponding user weighting based on user history
4. Calculates the similarities between users and items recipes
5. Ranks and delivers the Top N recipes for each user 

###  1.1. Loading, formatting and scaling 

We load the data from the CSV test data (this is now redundant as the format has changed). We then use concepts from information theory to scale the data and make the attribute weightings comparable. 

### 1.2. Removes noise and redundancy using dimension reduction (new)

There is a concept in statistics called Curse of Dimensionality. 
https://en.wikipedia.org/wiki/Curse_of_dimensionality
The occurs when data is very large and sparse (like our recipe data).   To help with this we use dimension reduction (e.g ALS or SVD) to clean and compress the data.  It also makes it a lot less computation expensive when working with the data. 

I chose Sparks SVD instead of ALS as its faster and has less parameters. 

### 1.3. User weights 

To computer user-item similarities, we need to learn a set of user weights. 

This is simply done by aggregating the user history for each user, and calculating the average attribute weight among the items that the user interacts with a recipe. This makes intuitive sense (the more someone interacts with an, the higher their preferences are for that item). We can then index/log of these averages (optional).  

The difficulty is doing this at scale, and making sure the weights make sense. This is where the SVD and Spark distributed computing helps. 

### 1.4. Users-items Similarities

For this we use cosine similarity.  This is a clever metric as it doesn't penalise items for exceeding in the same dimensions. For example, Euclidean distance would measure 2 dishes as very dissimilar just for being different quantities of the exact same dish.

![IMAGE ALT TEXT HERE](https://alexn.org/assets/img/similarity-graphic.png)

### 1.5. Ranks and delivers the Top N recipes 

Elasticsearch is every good at sorting and delivering data.  It s also the same infrastructure being used for the context filtering. 

I recently found the [Elasticsearch vector scoring plugin](https://github.com/MLnick/elasticsearch-vector-scoring).  This is specifically for evaluating and ranking with cosine similarity. So I have updated the code to use that (new). 


### 2.1 Clone this repo
Logn to your Ubuntu Ec2 instance. This also sets up port forwarding. 
```bash
ssh -L 8888:localhost:8888 ubuntu@<your-ec2-dns>.amazonaws.com -i /path/my-key-pair.pem
```

Install tools, 
```bash
sudo apt-get update
sudo apt-get install python-setuptools python-dev build-essential 
sudo apt install unzip
sudo apt-get install git
```

Download the repository
```bash
git clone https://rjbrooker2@bitbucket.org/rjbrooker2/emr-rec-n.git
```
### 2.2 Run the setup script. 
```bash
cd emr-rec-n
bash setup.sh
```

### 2.3 Start Elasticsearch Server
```bash
~/elasticsearch-5.3.0/bin/elasticsearch & 
```

### 2.4 Start jupyter 
Start the Jupyter IDE
```bash
PYSPARK_DRIVER_PYTHON="jupyter" PYSPARK_DRIVER_PYTHON_OPTS="notebook" ../spark-2.2.0-bin-hadoop2.7/bin/pyspark --driver-memory 4g --driver-class-path ../../elasticsearch-hadoop-5.3.0/dist/elasticsearch-spark-20_2.11-5.3.0.jar
```
Jupyter is now running on localhost:8888

## 3. Store database credentials
We store the database credentials as Linux variables. This is a lot safer than storing them in the python script.  

```bash
export rds_db_host=”<your_hostname>”
export rds_db_port=”<your_port>”
export rds_db_username=”<your_username>”
export rds_db_ password =”<your_ password >”
```




 